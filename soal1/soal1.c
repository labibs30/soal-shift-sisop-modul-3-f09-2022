#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>

pid_t pid;
pthread_t t_id[8];

char loc[] = "/home/labib";
char loc__[] = "/home/labib/music";
char loc__0[] = "/home/labib/quote";
char loc__1[] = "/home/labib/hasil";
char listFileMusic[500][500];
char listFileQuote[500][500];
char listTarjimMusic[500][500];
char listTarjimQuote[500][500];
char listPascaTarjimMusic[500][500];
char listPascaTarjimQuote[500][500];
int fileListMusic = 0;
int fileListQuote = 0;
void file(char *path, int p);
void decodeblock(unsigned char in[], char *clrstr);
void b64_decode(char *b64src, char *clrdst);
void *run(void *arg);
void *tarjim(void *arg);
void *hijrah(void *arg);
void *taqwah(void *arg);
void *iman(void *arg);

void *run(void *arg)
{
    char *url;
    url = (char *)arg;

    printf("url adalah = %s\n", url);
    pthread_t id = pthread_self();

    if(pthread_equal(id, t_id[0])){
        pid = fork();
        if(pid == 0){
            char *argc[] = {"wget", "-q", "--no-check-certificate", url, "-O", "/home/labib/quote.zip", NULL};
            execv("/usr/bin/wget", argc);
        }
        while((wait(NULL))>0);

        pid = fork();
        if(pid == 0){
            chdir("/home/labib/quote");
            char *argc[]={"unzip","../quote.zip", NULL};
            execv("/usr/bin/unzip",argc);
        }
    }
    else if(pthread_equal(id, t_id[1])){
        pid = fork();
        if(pid == 0){
            char *argc[] = {"wget", "-q", "--no-check-certificate", url, "-O", "/home/labib/music.zip", NULL};
            execv("/usr/bin/wget", argc);
        }
        while((wait(NULL))>0);

        pid = fork();
        if(pid == 0){
            chdir("/home/labib/music");
            char *argc[]={"unzip","../music.zip", NULL};
            execv("/usr/bin/unzip",argc);
        }
    }
    pthread_exit(NULL);
    return NULL;

}
void file(char *path, int p){
    DIR *folder;
    struct dirent *entry;
    char *token;
    folder = opendir(path);
    if(folder == NULL)
    {
        perror("Unable to read directory");
        exit(EXIT_FAILURE);
    }

    while( (entry=readdir(folder)) )
    {   
        if(strstr(entry->d_name, ".") && strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
            token = strtok(entry->d_name, "_");

            while( token != NULL){
                if(p == 1){
                    strcpy(listFileMusic[fileListMusic++], token);
                    token = strtok(NULL, "_");
                }
                else if(p == 2){
                    strcpy(listFileQuote[fileListQuote++], token);
                    token = strtok(NULL, "_");      
                }

            }
        }
    }
    closedir(folder);
}
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  clrdst[0] = '\0';
  phase = 0; i=0;
  while(b64src[i]) {
    c = (int) b64src[i];
    if(c == '=') {
      decodeblock(in, clrdst); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, clrdst);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
}
void *tarjim(void *arg)
{   
    char *dir;
    dir = (char *)arg;

    pthread_t id = pthread_self();
    if(pthread_equal(id, t_id[2])){
        FILE* ptr;
        FILE* fptr;
        char ch;
        char dest__[600], dest__1[600];
        file(loc__, 1);
        sprintf(dest__1,"%s/%s.txt", loc__, dir);

        fptr = fopen(dest__1, "w+");
     
        for (int i = 0; i < fileListMusic; i++)
        {   
            int len = 0;
            sprintf(dest__, "%s/%s",loc__, listFileMusic[i]);
            ptr = fopen(dest__, "r");

            if (NULL == ptr) {
                printf("file can't be opened \n");
            }
            ch = fgetc(ptr);
            while(ch != EOF && ch!='\n'){
                listTarjimMusic[i][len] = ch;
                len++;
                ch = fgetc(ptr);
            }
            b64_decode(listTarjimMusic[i], listPascaTarjimMusic[i]);
            printf("%s\n",listPascaTarjimMusic[i]);
            fclose(ptr);
        }
        for (int i = 0; i < fileListMusic; i++)
        {   
            if (strlen(listPascaTarjimMusic[i]) > 0)
            {
                fprintf(fptr, "%s\n", listPascaTarjimMusic[i]);
            }
        }
        fclose(fptr);
    }
    else if(pthread_equal(id, t_id[3])){
        FILE* ptr;
        FILE* fptr;
        char ch;
        char dest__[600], dest__1[600];
        file(loc__0,2);
        sprintf(dest__1,"%s/%s.txt", loc__0, dir);

        fptr = fopen(dest__1, "w+");

        for (int i = 0; i < fileListQuote; i++)
        {   
            int len = 0;
            sprintf(dest__, "%s/%s",loc__0, listFileQuote[i]);
            ptr = fopen(dest__, "r");

            if (NULL == ptr) {
                printf("file can't be opened \n");
            }
            ch = fgetc(ptr);
            while(ch != EOF && ch!='\n'){
                listTarjimQuote[i][len] = ch;
                len++;
                ch = fgetc(ptr);
            }
            b64_decode(listTarjimQuote[i], listPascaTarjimQuote[i]);
            printf("%s\n",listPascaTarjimQuote[i]);
            fclose(ptr);
        }
        for (int i = 0; i < fileListQuote; i++)
        {   
            if (strlen(listPascaTarjimQuote[i])>0)
            {
                fprintf(fptr, "%s\n", listPascaTarjimQuote[i]);
            }
            
        }
        fclose(fptr);
    }
    pthread_exit(NULL);
    return NULL;
}
void *hijrah(void *arg)
{
    pthread_t id = pthread_self();
    if(pthread_equal(id, t_id[4])){
        pid = fork();
        char src__m[200];
        sprintf(src__m, "%s/%s.txt", loc__, "music");
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            char *argv[] = {"mv", src__m, loc__1, NULL};
            execv("/usr/bin/mv", argv);
        }
    }
    else if(pthread_equal(id, t_id[5])){
        pid = fork();
        char src__q[200];
        sprintf(src__q, "%s/%s.txt", loc__0, "quote");
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            char *argv1[] = {"mv", src__q, loc__1, NULL};
            execv("/usr/bin/mv", argv1);
        }
    }
    pthread_exit(NULL);
    return NULL;
}
void *taqwah(void *arg)
{   
    char dest__[200];
    pid = fork();
    if (pid < 0) exit(EXIT_FAILURE);

    if(pid == 0){
        chdir("/home/labib/");
        char *argv1[] = {"zip","-r","-P","mihinomenestlabib", "hasil.zip","hasil", NULL};
        execv("/usr/bin/zip", argv1);
    }
    pthread_exit(NULL);
    return NULL;
}
void *iman(void *arg)
{
    pid = fork();

    if(pid == 0){
        chdir("/home/labib/unzip");
        char *argc[]={"unzip","../hasil.zip", NULL};
        execv("/usr/bin/unzip",argc);
    }
    while((wait(NULL)) > 0);
    pid = fork();
    if(pid == 0){
        FILE* ptr;
        ptr = fopen("/home/labib/unzip/hasil/no.txt", "w+");
        fprintf(ptr, "No");
        fclose(ptr);
    }
    while((wait(NULL)) > 0);
    pid = fork();
    if(pid == 0){
        chdir("/home/labib/unzip");
        char *argv[] = {"zip","-r","-P","mihinomenestlabib", "hasil.zip", "hasil",  NULL};
        execv("/bin/zip", argv);
    }

}
int main(int argc, char const *argv[])
{
    char quote[] = "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download";
    char music[] = "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download";
    char msg1[] = "music";
    char msg2[] = "quote";
    printf("Membuat Thread Baru\n");
    int i = 0;
    while (i < 8)
    {   
        if(i == 0){
            pthread_create(&t_id[i], NULL, &run ,&quote);
            sleep(2);
        }
        else if(i == 1){
            pthread_create(&t_id[i], NULL, &run ,&music);
            sleep(2);
        }
        else if(i == 2){
            pthread_create(&t_id[i], NULL, &tarjim, &msg1);
            sleep(2);
        }
        else if(i == 3){
            pthread_create(&t_id[i], NULL, &tarjim, &msg2);
            sleep(2);
        }
        else if(i == 4){
            pthread_create(&t_id[i], NULL, &hijrah, NULL);
            sleep(2);
        }
        else if(i == 5){
            pthread_create(&t_id[i], NULL, &hijrah, NULL);
            sleep(2);
        }
        else if(i == 6){
            pthread_create(&t_id[i], NULL, &taqwah, NULL);
            sleep(2);
        }
        else if(i == 7){
            pthread_create(&t_id[i], NULL, &iman, NULL);
            sleep(2);
        }
        i++;
    }

    pthread_join(t_id[0], NULL);
    pthread_join(t_id[1], NULL);
    pthread_join(t_id[2], NULL);
    pthread_join(t_id[3], NULL);
    pthread_join(t_id[4], NULL);
    pthread_join(t_id[5], NULL);
    pthread_join(t_id[6], NULL);
    pthread_join(t_id[7], NULL);

    printf("Thread Selesai");
    return 0;
}
