#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#define SIZE 1024

void send_file(FILE *fp, int sockfd){
  int n;
  char data[SIZE] = {0};

  while(fgets(data, SIZE, fp) != NULL) {
    if (send(sockfd, data, sizeof(data), 0) == -1) {
      perror("[-]Error in sending file.");
      exit(1);
    }
    bzero(data, SIZE);
  }
}

int main(){
  char *ip = "127.0.0.1";
  int port = 8080;
  int e;
  char command[50];

  int sockfd;
  struct sockaddr_in server_addr;
  FILE *fp;
  char *filename = "hartakarun.zip";
  pid_t child_id;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+]Server socket created successfully.\n");

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port;
  server_addr.sin_addr.s_addr = inet_addr(ip);

  e = connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
  if(e == -1) {
    perror("[-]Error in socket");
    exit(1);
  }
	printf("[+]Connected to Server.\n");

  fp = fopen(filename, "r");
  if (fp == NULL) {
    perror("[-]Error in reading file.");
    exit(1);
  }
  scanf("%s", command);
  if(strstr(command, "send hartakarun.zip")){
    send_file(fp, sockfd);
  }
  
  printf("[+]File data sent successfully.\n");

	printf("[+]Closing the connection.\n");
  close(sockfd);

  return 0;
}
























// child_id = fork();
// 	if(child_id < 0)
//  	{
// 		exit(EXIT_FAILURE);
// 	}

// 	if(child_id == 0)
// 	{
// 		char *argv[] = {"zip", "-r", "/Users/elektroforesis/bismillah/soal-shift-sisop-modul-3-f09-2022/soal3/client/hartakarun.zip", "/Users/elektroforesis/shift3/hartakarun", NULL};
// 		if(execvp("/bin/zip", argv) == -1) {
// 			perror("zip failed!");
// 			exit(EXIT_FAILURE);
// 		}
// 	}
