#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <dirent.h>
#include <libgen.h>
#include <pthread.h>
#include <errno.h>
const char *prg;

static void safe_create_dir(const char *dir) {
	if(mkdir(dir,0755) < 0) {
		if (errno != EEXIST) {
			perror(dir);
			exit(1);
		}
	}
}

void stringLwr(char *s) {
	int i=0;
	while(s[i]!='\0') {
	if(s[i]>='A' && s[i]<='Z') {
		s[i]=s[i]+32;
	}
	i++;
	}
}

int is_hidden(const char *name)
{
	return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}

char *get_filename_ext(char *filename) {
	struct stat st;
	char *dot = strrchr(filename, '.');
	if (is_hidden(filename)) return "Hidden";
	if(!dot || dot == filename) return "Unknown";
	else return dot+1;
}

void *auto_check(void *args) {
char path_des[1000];
	char *path_src = (char*)malloc(sizeof(char));
	path_src = (char*)args;
	char *bname = basename(path_src);
	char *ext =strdup(get_filename_ext(bname));
	//printf("%s\n", bname);
	stringLwr(ext);
	if(strcmp(ext,"gz")== 0) {
		ext = "tar.gz";
	}
	strcpy(path_des, "/Users/elektroforesis/shift3/hartakarun");
	strcat(path_des, "/");
	strcat(path_des, ext);
	strcat(path_des, "/");
	safe_create_dir(path_des);
	strcat(path_des, bname);
	rename(path_src, path_des);
}
int isDirectory(const char*path) {
	struct stat statbuf;
	if(stat(path, &statbuf) != 0)
		return 0;
	return S_ISDIR(statbuf.st_mode);
}
void listFilesRecursively(char *basePath)
{
	char path_src[1000];
	struct dirent *dp;
	struct  stat statbuf;
	pthread_t t_id[200];
	int i=0;
	DIR *dir = opendir(basePath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0)
		{

			strcpy(path_src, basePath);
			strcat(path_src, "/");
			strcat(path_src, dp->d_name);

			listFilesRecursively(path_src);
			if (isDirectory(path_src))
			{
			rmdir(path_src);
			}
			else {
			pthread_create(&t_id[i], NULL, &auto_check, (void *)path_src);
			pthread_join(t_id[i], NULL);
			i++;
			}
		}
	}
	closedir(dir);
}

int main(int argc, char *argv[]) {

listFilesRecursively("/Users/elektroforesis/shift3/hartakarun");

return 0;
}
