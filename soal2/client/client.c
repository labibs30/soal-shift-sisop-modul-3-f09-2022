#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h> 
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
#define SIZE_STR 1024
int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if((sock=socket(AF_INET, SOCK_STREAM, 0))<0){
        printf("\n socket creation error\n");
        return -1;
    }    
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0){
        printf("\nInvalid addres / Adrees not supported\n");
        return -1;
    }
    if(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0){
        printf("\nConnection Failed\n");
        return -1;
    }

    //printf("Hello message sent\n");
    valread = read(sock, buffer, 1024);
    int opt;
    char login[SIZE_STR] = "Login";
    char regist[SIZE_STR] = "Register";
    char username[SIZE_STR];
    char password[SIZE_STR];
    char check[SIZE_STR];
    int state = 0;
    while(strcmp("Connected to the server", buffer)!=0){
        printf("Check server is not full");
        scanf("%s",check);
        send(sock,check, SIZE_STR, 0);
        valread = read(sock, buffer, SIZE_STR);
        
    }
    if(strcmp("Connected to the server", buffer)!=0){
        perror("ERROR : OUT OF BOUNDS");
        return 0;
    }
    printf("\n WELCOME USER \n");
    while(1){
        
        printf("1. Register\n2. Login\n");
        printf("Enter option number : ");
        scanf("%d", &opt);
        
        if(opt == 1){
            send(sock, regist, strlen(regist), 0);

            while(1){
                printf("Enter new username : ");
                scanf("%s", username);
                send(sock, username, SIZE_STR, 0);
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);

                if(strcmp(buffer, "Username Invalid") == 0){
                    printf("Username already exists! Try to submit another\n\n");
                }else if(strcmp(buffer, "Username Valid")==0)break;
            }
            while(1){
                printf("Password (longer than 6 characters and at least 1 (number, upper case, lower case) : ");
                scanf("%s", password);
                send(sock, password, SIZE_STR, 0);
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);

                if(strcmp(buffer, "Password Invalid")==0){
                    printf("Password Invalid, Retry another instead\n");
                }else if(strcmp(buffer, "Password Valid")==0){
                    state = 1;

                    printf("User is registered \n");
                    break;
                }
            }
        }
        else if(opt == 2){
            send(sock, login, strlen(login),0);
            printf("Username : ");
            scanf("%s", username);
            send(sock, username, SIZE_STR, 0);

            printf("Password :");
            scanf("%s", password);
            send(sock, password, SIZE_STR, 0);
            printf("\n");
            memset(buffer, 0, sizeof(buffer));
            valread = read(sock, buffer, SIZE_STR);

            if(strcmp("Login success", buffer)==0){
                state = 1;
                printf("--> Login succed <--");
            }else{
                printf("Wrong Username or Password! Retry\n\n");
            }

        }
    }
    printf("sdf %s\n", buffer);

    return 0;
}
