#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#define PORT 8080
#define SIZE_STR 1024
int totalThread = 0;
typedef struct user_account
{
    char username[SIZE_STR];
    char password[SIZE_STR];
    /* data */
}account;

int checkIfValid(char *pass){
    if(strlen(pass) < 6) return 0;

    int containsNumber = 0, containsUpper = 0, containsLower = 0;
    int i = 0;
    while(i < strlen(pass)){
        if(pass[i] >= '0' && pass[i] <= '9') containsNumber =1;
        if(pass[i] >= 'A' && pass[i] <= 'Z') containsUpper =1;
        if(pass[i] >= 'a' && pass[i] <= 'z') containsLower =1;
        i++;
    }
    if(containsLower == 0 || containsNumber == 0|| containsUpper == 0)return 0;
    return 1;
}
void *client(void *socket){
    int valread;
    char buffer[SIZE_STR] = {0};
    char loc[100] = "/home/labib/users.txt";
    int state = 0;

    int new_socket = *(int*)socket;

    account userAccount;
    FILE *userTxt;

    if(totalThread == 1){
        userTxt = fopen(loc, "a");
        fclose(userTxt);
        send(new_socket, "Connected to the server", SIZE_STR, 0);
    }else{
        send(new_socket, "Server is full, please wait", SIZE_STR, 0);
    }
    while(totalThread > 1){
        valread = read(new_socket, buffer, SIZE_STR);
        if(totalThread == 1){
            send(new_socket, "Connected to the server", SIZE_STR, 0);
        }else{
            send(new_socket, "Server is full, please wait", SIZE_STR, 0);
        }
    }
    while (1)
    {   
        valread = read(new_socket, buffer, SIZE_STR);

        if(strcmp(buffer, "Login") == 0){
            printf("SERVER LOG : User Try to Login");
            userTxt = fopen(loc, "r");
            fclose(userTxt);
            valread = read(new_socket, userAccount.username, SIZE_STR);
            valread = read(new_socket, userAccount.password, SIZE_STR);
            printf("SERVER LOG : TRY INFO [%s:%s]", userAccount.username, userAccount.password);

            char accountData[SIZE_STR];
            char tempUsername[SIZE_STR];
            char tempPassword[SIZE_STR];

            int accountFound = 0;

            while(fgets(accountData, SIZE_STR, userTxt)!=NULL){
                int i = 0;
                while(accountData[i]!=':'){
                    tempUsername[i] = accountData[i];
                    i++;
                }
                tempUsername[i] = '\0';
                i++;
                int j = 0;
                while(accountData[i] != '\n'){
                    tempPassword[j] = accountData[i];
                    i++;
                    j++;
                }
                tempPassword[j]='\0';

                if(strcmp(tempUsername, userAccount.username) == 0){
                    if(strcmp(tempPassword, userAccount.password) == 0){
                        accountFound = 1;
                        break;
                    }
                }
            }
            if(accountFound == 0){
                send(new_socket, "Login failed", SIZE_STR, 0);
            }else{
                send(new_socket, "Login success", SIZE_STR, 0);
                fclose(userTxt);
                printf("SERVER LOG : USER INFO [%s:%s]", userAccount.username, userAccount.password);
                state = 1;
            }
        }
        else if(strcmp(buffer, "Register") == 0) {
            printf("SERVER LOG: User try to register\n");

            while(1) {
                userTxt = fopen(loc, "r");
                valread = read(new_socket, userAccount.username, SIZE_STR);

                // variables to check validation
                char accountData[SIZE_STR];
                char tempUsername[SIZE_STR];

                int usernameFound = 0;

                // check username validation
                while(fgets(accountData, SIZE_STR, userTxt) != NULL) {
                    int i = 0;
                    while(accountData[i] != ':') {
                        tempUsername[i] = accountData[i];
                        i++;
                    }
                    tempUsername[i] = '\0';

                    if(strcmp(tempUsername, userAccount.username) == 0) {
                        printf("SERVER LOG: Username already been taken\n");
                        usernameFound = 1;
                        send(new_socket, "Username Invalid", SIZE_STR, 0);
                        break;
                    }
                }

                if(usernameFound == 0) {
                    fclose(userTxt);
                    break;
                }
            }
            // if username is valid, check pass
            send(new_socket, "Username Valid", SIZE_STR, 0);
            while(1) {
                valread = read(new_socket, userAccount.password, SIZE_STR);
                // if invalid
                if(checkIfValid(userAccount.password) == 0) {
                    send(new_socket, "Password Invalid", SIZE_STR, 0);
                    continue;
                }
                // if valid
                else {
                    send(new_socket, "Password Valid", SIZE_STR, 0);
                    userTxt = fopen(loc, "a");
                    fprintf(userTxt, "%s:%s\n", userAccount.username, userAccount.password);
                    fclose(userTxt);

                    printf("SERVER LOG: New User Registered [%s:%s]\n", userAccount.username, userAccount.password);
                    
                    state= 1;
                    break;
                }
            }
        }

    }
}
int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server sadasd";
    pthread_t tid[10];
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    while (1)
    {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        pthread_create(&tid[totalThread], NULL, &client, &new_socket);
        totalThread++;

        /* code */
    }
    
    return 0;
}