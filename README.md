# soal-shift-sisop-modul-2-F09-2022

Anggota Kelompok :
* Gaudhiwaa Hendrasto	5025201066
* M Labib Alfaraby      5025201083

1. Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

	**a**. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

	**b**. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

	**c**. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

	**d**. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

	**e**. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

### Penyelesaian Soal 1

- `Nomor 1a` Pada soal a, pendowloadan dilakukan menggunakan wget, melalui execv

```c
void *run(void *arg)
{
    char *url;
    url = (char *)arg;

    printf("url adalah = %s\n", url);
    pthread_t id = pthread_self();

    if(pthread_equal(id, t_id[0])){
        pid = fork();
        if(pid == 0){
            char *argc[] = {"wget", "-q", "--no-check-certificate", url, "-O", "/home/labib/quote.zip", NULL};
            execv("/usr/bin/wget", argc);
        }
        while((wait(NULL))>0);

        pid = fork();
        if(pid == 0){
            chdir("/home/labib/quote");
            char *argc[]={"unzip","../quote.zip", NULL};
            execv("/usr/bin/unzip",argc);
        }
    }
    else if(pthread_equal(id, t_id[1])){
        pid = fork();
        if(pid == 0){
            char *argc[] = {"wget", "-q", "--no-check-certificate", url, "-O", "/home/labib/music.zip", NULL};
            execv("/usr/bin/wget", argc);
        }
        while((wait(NULL))>0);

        pid = fork();
        if(pid == 0){
            chdir("/home/labib/music");
            char *argc[]={"unzip","../music.zip", NULL};
            execv("/usr/bin/unzip",argc);
        }
    }
    pthread_exit(NULL);
    return NULL;

}
![hasil](./screenshot/1a.png)
![hasil](./screenshot/1a1.png)
```
- `Nomor 1b` Pada soal b, pertama kita lakukan listing file untuk masing - masing folder, yaitu music dan quote menggunakan fungsi file, sama seperti listing file modul 2

```c
void file(char *path, int p){
    DIR *folder;
    struct dirent *entry;
    char *token;
    folder = opendir(path);
    if(folder == NULL)
    {
        perror("Unable to read directory");
        exit(EXIT_FAILURE);
    }

    while( (entry=readdir(folder)) )
    {   
        if(strstr(entry->d_name, ".") && strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
            token = strtok(entry->d_name, "_");

            while( token != NULL){
                if(p == 1){
                    strcpy(listFileMusic[fileListMusic++], token);
                    token = strtok(NULL, "_");
                }
                else if(p == 2){
                    strcpy(listFileQuote[fileListQuote++], token);
                    token = strtok(NULL, "_");      
                }

            }
        }
    }
    closedir(folder);
}

```
Lalu, apabila telah mempunyai list file tersebut, kita dapat manfaatkan untuk dijadikan input read file serta melakukan decode untuk masing - masing file. Hasil decode akan disimpan pada file txt masing - masing folder. Berikut fungsi untuk melakukan decode, beserta penyimpanan hasil decode ke dalam file txt.
```c
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  clrdst[0] = '\0';
  phase = 0; i=0;
  while(b64src[i]) {
    c = (int) b64src[i];
    if(c == '=') {
      decodeblock(in, clrdst); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, clrdst);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
}
void *tarjim(void *arg)
{   
    char *dir;
    dir = (char *)arg;

    pthread_t id = pthread_self();
    if(pthread_equal(id, t_id[2])){
        FILE* ptr;
        FILE* fptr;
        char ch;
        char dest__[600], dest__1[600];
        file(loc__, 1);
        sprintf(dest__1,"%s/%s.txt", loc__, dir);

        fptr = fopen(dest__1, "w+");
     
        for (int i = 0; i < fileListMusic; i++)
        {   
            int len = 0;
            sprintf(dest__, "%s/%s",loc__, listFileMusic[i]);
            ptr = fopen(dest__, "r");

            if (NULL == ptr) {
                printf("file can't be opened \n");
            }
            ch = fgetc(ptr);
            while(ch != EOF && ch!='\n'){
                listTarjimMusic[i][len] = ch;
                len++;
                ch = fgetc(ptr);
            }
            b64_decode(listTarjimMusic[i], listPascaTarjimMusic[i]);
            printf("%s\n",listPascaTarjimMusic[i]);
            fclose(ptr);
        }
        for (int i = 0; i < fileListMusic; i++)
        {   
            if (strlen(listPascaTarjimMusic[i]) > 0)
            {
                fprintf(fptr, "%s\n", listPascaTarjimMusic[i]);
            }
        }
        fclose(fptr);
    }
    else if(pthread_equal(id, t_id[3])){
        FILE* ptr;
        FILE* fptr;
        char ch;
        char dest__[600], dest__1[600];
        file(loc__0,2);
        sprintf(dest__1,"%s/%s.txt", loc__0, dir);

        fptr = fopen(dest__1, "w+");

        for (int i = 0; i < fileListQuote; i++)
        {   
            int len = 0;
            sprintf(dest__, "%s/%s",loc__0, listFileQuote[i]);
            ptr = fopen(dest__, "r");

            if (NULL == ptr) {
                printf("file can't be opened \n");
            }
            ch = fgetc(ptr);
            while(ch != EOF && ch!='\n'){
                listTarjimQuote[i][len] = ch;
                len++;
                ch = fgetc(ptr);
            }
            b64_decode(listTarjimQuote[i], listPascaTarjimQuote[i]);
            printf("%s\n",listPascaTarjimQuote[i]);
            fclose(ptr);
        }
        for (int i = 0; i < fileListQuote; i++)
        {   
            if (strlen(listPascaTarjimQuote[i])>0)
            {
                fprintf(fptr, "%s\n", listPascaTarjimQuote[i]);
            }
            
        }
        fclose(fptr);
    }
    pthread_exit(NULL);
    return NULL;
}

```
![hasil](./screenshot/1b.png)
![hasil](./screenshot/1b1.png)
- `Nomor 1c` Pada soal 1c, pemindahan file dilakukan dengan menggunakan `mv` melalui `execv`.
```c
void *hijrah(void *arg)
{
    pthread_t id = pthread_self();
    if(pthread_equal(id, t_id[4])){
        pid = fork();
        char src__m[200];
        sprintf(src__m, "%s/%s.txt", loc__, "music");
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            char *argv[] = {"mv", src__m, loc__1, NULL};
            execv("/usr/bin/mv", argv);
        }
    }
    else if(pthread_equal(id, t_id[5])){
        pid = fork();
        char src__q[200];
        sprintf(src__q, "%s/%s.txt", loc__0, "quote");
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            char *argv1[] = {"mv", src__q, loc__1, NULL};
            execv("/usr/bin/mv", argv1);
        }
    }
    pthread_exit(NULL);
    return NULL;
}

```
- `Nomor 1d` Pada soal 1d, zipping file dilakukan menggunakan `zip` melalui `execv`, untuk memberikan password pada zip file dapat menggunakan opsi `-P` dilanjutkan dengan passwordnya, contoh pada dokumentasi adalah `mihinomenestlabib`. Berikut fungsi yang mengeksekusinya.
```c
void *taqwah(void *arg)
{   
    char dest__[200];
    pid = fork();
    if (pid < 0) exit(EXIT_FAILURE);

    if(pid == 0){
        chdir("/home/labib/");
        char *argv1[] = {"zip","-r","-P","mihinomenestlabib", "hasil.zip","hasil", NULL};
        execv("/usr/bin/zip", argv1);
    }
    pthread_exit(NULL);
    return NULL;
}

```
![hasil](./screenshot/1d.png)
- `Nomor 1e` Pada 1e, kita diminta untuk unzip file hasil terlebih dahulu lalu menyisipkan file txt baru dengan nama no.txt yang berisi string No dan akan dilakukan zipping kembali serta diberikan password kembali dengan password yang  sama seperti sebelumnya. Untuk melakukan unzip file dapat menggunakan `unzip` melalui `execv`, lalu zip dengan cara yang sama seperti pada `soal 1d`, berikut fungsi yang mengeksekusinya.
```c
void *iman(void *arg)
{
    pid = fork();

    if(pid == 0){
        chdir("/home/labib/unzip");
        char *argc[]={"unzip","../hasil.zip", NULL};
        execv("/usr/bin/unzip",argc);
    }
    while((wait(NULL)) > 0);
    pid = fork();
    if(pid == 0){
        FILE* ptr;
        ptr = fopen("/home/labib/unzip/hasil/no.txt", "w+");
        fprintf(ptr, "No");
        fclose(ptr);
    }
    while((wait(NULL)) > 0);
    pid = fork();
    if(pid == 0){
        chdir("/home/labib/unzip");
        char *argv[] = {"zip","-r","-P","mihinomenestlabib", "hasil.zip", "hasil",  NULL};
        execv("/bin/zip", argv);
    }

}

```
![hasil](./screenshot/1e.png)
***
2. Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

	**a**. Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

		- Username unique (tidak boleh ada user yang memiliki username yang sama)

		- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil


	**b**.

	**c**.

	**d**.

	**e**.

	**f**.

	**g**.

### Penyelesaian Soal 2
- `Nomor 2a ` Pada saat client terhubung ke dalam server, client dapat memilih 2 opsi yaitu login atau register. Apabila memilih register, maka client akan diminta untuk melakukan input username dan password yang nantinya akan disimpan ke dalam file users.txt dengan struktur username:password, password yang valid memiliki beberapa kriteria yang harus dipenuhi apabila tidak sesuai maka registrasi belum berhasil. Sedangkan, apabila client memilih login maka client akan diminta untuk melakukan input username dan password juga, dan akan dilakukan pengecekan apakah username ada serta password sesuai.

`CLIENT`
```c
valread = read(sock, buffer, 1024);
    int opt;
    char login[SIZE_STR] = "Login";
    char regist[SIZE_STR] = "Register";
    char username[SIZE_STR];
    char password[SIZE_STR];
    char check[SIZE_STR];
    int state = 0;
    while(strcmp("Connected to the server", buffer)!=0){
        printf("Check server is not full");
        scanf("%s",check);
        send(sock,check, SIZE_STR, 0);
        valread = read(sock, buffer, SIZE_STR);
        
    }
    if(strcmp("Connected to the server", buffer)!=0){
        perror("ERROR : OUT OF BOUNDS");
        return 0;
    }
    printf("\n WELCOME USER \n");
    while(1){
        
        printf("1. Register\n2. Login\n");
        printf("Enter option number : ");
        scanf("%d", &opt);
        
        if(opt == 1){
            send(sock, regist, strlen(regist), 0);

            while(1){
                printf("Enter new username : ");
                scanf("%s", username);
                send(sock, username, SIZE_STR, 0);
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);

                if(strcmp(buffer, "Username Invalid") == 0){
                    printf("Username already exists! Try to submit another\n\n");
                }else if(strcmp(buffer, "Username Valid")==0)break;
            }
            while(1){
                printf("Password (longer than 6 characters and at least 1 (number, upper case, lower case) : ");
                scanf("%s", password);
                send(sock, password, SIZE_STR, 0);
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);

                if(strcmp(buffer, "Password Invalid")==0){
                    printf("Password Invalid, Retry another instead\n");
                }else if(strcmp(buffer, "Password Valid")==0){
                    state = 1;

                    printf("User is registered \n");
                    break;
                }
            }
        }
        else if(opt == 2){
            send(sock, login, strlen(login),0);
            printf("Username : ");
            scanf("%s", username);
            send(sock, username, SIZE_STR, 0);

            printf("Password :");
            scanf("%s", password);
            send(sock, password, SIZE_STR, 0);
            printf("\n");
            memset(buffer, 0, sizeof(buffer));
            valread = read(sock, buffer, SIZE_STR);

            if(strcmp("Login success", buffer)==0){
                state = 1;
                printf("--> Login succed <--");
            }else{
                printf("Wrong Username or Password! Retry\n\n");
            }

        }
    }

```
`SERVER` Terdapat fungsi `checkIfValid` yang berfungsi untuk mengecek apakah password yang dimasukkan ketika register telah sesuai atau belum.
```c
int totalThread = 0;
typedef struct user_account
{
    char username[SIZE_STR];
    char password[SIZE_STR];
    /* data */
}account;

int checkIfValid(char *pass){
    if(strlen(pass) < 6) return 0;

    int containsNumber = 0, containsUpper = 0, containsLower = 0;
    int i = 0;
    while(i < strlen(pass)){
        if(pass[i] >= '0' && pass[i] <= '9') containsNumber =1;
        if(pass[i] >= 'A' && pass[i] <= 'Z') containsUpper =1;
        if(pass[i] >= 'a' && pass[i] <= 'z') containsLower =1;
        i++;
    }
    if(containsLower == 0 || containsNumber == 0|| containsUpper == 0)return 0;
    return 1;
}
void *client(void *socket){
    int valread;
    char buffer[SIZE_STR] = {0};
    char loc[100] = "/home/labib/users.txt";
    int state = 0;

    int new_socket = *(int*)socket;

    account userAccount;
    FILE *userTxt;

    if(totalThread == 1){
        userTxt = fopen(loc, "a");
        fclose(userTxt);
        send(new_socket, "Connected to the server", SIZE_STR, 0);
    }else{
        send(new_socket, "Server is full, please wait", SIZE_STR, 0);
    }
    while(totalThread > 1){
        valread = read(new_socket, buffer, SIZE_STR);
        if(totalThread == 1){
            send(new_socket, "Connected to the server", SIZE_STR, 0);
        }else{
            send(new_socket, "Server is full, please wait", SIZE_STR, 0);
        }
    }
    while (1)
    {   
        valread = read(new_socket, buffer, SIZE_STR);

        if(strcmp(buffer, "Login") == 0){
            printf("SERVER LOG : User Try to Login");
            userTxt = fopen(loc, "r");
            fclose(userTxt);
            valread = read(new_socket, userAccount.username, SIZE_STR);
            valread = read(new_socket, userAccount.password, SIZE_STR);
            printf("SERVER LOG : TRY INFO [%s:%s]", userAccount.username, userAccount.password);

            char accountData[SIZE_STR];
            char tempUsername[SIZE_STR];
            char tempPassword[SIZE_STR];

            int accountFound = 0;

            while(fgets(accountData, SIZE_STR, userTxt)!=NULL){
                int i = 0;
                while(accountData[i]!=':'){
                    tempUsername[i] = accountData[i];
                    i++;
                }
                tempUsername[i] = '\0';
                i++;
                int j = 0;
                while(accountData[i] != '\n'){
                    tempPassword[j] = accountData[i];
                    i++;
                    j++;
                }
                tempPassword[j]='\0';

                if(strcmp(tempUsername, userAccount.username) == 0){
                    if(strcmp(tempPassword, userAccount.password) == 0){
                        accountFound = 1;
                        break;
                    }
                }
            }
            if(accountFound == 0){
                send(new_socket, "Login failed", SIZE_STR, 0);
            }else{
                send(new_socket, "Login success", SIZE_STR, 0);
                fclose(userTxt);
                printf("SERVER LOG : USER INFO [%s:%s]", userAccount.username, userAccount.password);
                state = 1;
            }
        }
        else if(strcmp(buffer, "Register") == 0) {
            printf("SERVER LOG: User try to register\n");

            while(1) {
                userTxt = fopen(loc, "r");
                valread = read(new_socket, userAccount.username, SIZE_STR);

                // variables to check validation
                char accountData[SIZE_STR];
                char tempUsername[SIZE_STR];

                int usernameFound = 0;

                // check username validation
                while(fgets(accountData, SIZE_STR, userTxt) != NULL) {
                    int i = 0;
                    while(accountData[i] != ':') {
                        tempUsername[i] = accountData[i];
                        i++;
                    }
                    tempUsername[i] = '\0';

                    if(strcmp(tempUsername, userAccount.username) == 0) {
                        printf("SERVER LOG: Username already been taken\n");
                        usernameFound = 1;
                        send(new_socket, "Username Invalid", SIZE_STR, 0);
                        break;
                    }
                }

                if(usernameFound == 0) {
                    fclose(userTxt);
                    break;
                }
            }
            // if username is valid, check pass
            send(new_socket, "Username Valid", SIZE_STR, 0);
            while(1) {
                valread = read(new_socket, userAccount.password, SIZE_STR);
                // if invalid
                if(checkIfValid(userAccount.password) == 0) {
                    send(new_socket, "Password Invalid", SIZE_STR, 0);
                    continue;
                }
                // if valid
                else {
                    send(new_socket, "Password Valid", SIZE_STR, 0);
                    userTxt = fopen(loc, "a");
                    fprintf(userTxt, "%s:%s\n", userAccount.username, userAccount.password);
                    fclose(userTxt);

                    printf("SERVER LOG: New User Registered [%s:%s]\n", userAccount.username, userAccount.password);
                    
                    state= 1;
                    break;
                }
            }
        }

    }
}

```
***
3. Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan
    
    **a**. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif**.
    
    **b**. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”. **
    
    **c**. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.**
   
    **d**. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.**
    
    **e**. Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server send hartakarun.zip Contoh : 157_characters_5_Albedo_53880**

    Karena Nami tidak bisa membuat programnya, maka Nami meminta bantuanmu untuk membuat programnya. Bantulah Nami agar programnya dapat berjalan!

### Penyelesaian Soal 3
- `Nomor 3a`
Folder shift3 dibuat terlebih dahulu. Untuk membuat folder shift3 di /home/[user] dengan command line :

```c
mkdir shift3
```

Kemudian untuk melakukan extract file "hartakarun.zip" dapat dilakukan secara manual dengan menggunakan bantuan bash pada command line di linux. Command untuk melakukan extract file dan memindahkannya ke directory yang diinginkan adalah sebagai berikut.

```c
unzip -q ~/Downloads/hartakarun.zip -d ~/shift3
```

Setelah itu, program pengkategorian berbahasa c harus ditelakkan ke dalam folder hasil ekstraksi "hartakarun.zip." Hal ini dapat dilakukan dengan menggunakan bantuan command cp atau mv.

Untuk melakukan pengkategorian file berdasarkan ekstensinya, program akan melakukan traverse ke setiap sub folder hingga mendapatkan semua file yang terdapat dalam setiap sub-folder maupun yang di luar sub folder.
Apabila program menemukan sebuah file, maka program akan membentuk sebuah thread untuk melakukan pemeriksaan terkait ekstensi file tersebut, membuat folder sesuai dengan ekstensi file, dan memindahkan file tersebut ke folder terkait
Karena program yang dibuat diharapkan bersifat tidak case sensitive, maka setiap ekstensi file yang didapatkan harus diubah ke lower case dengan menggunakan fungsi stringLwr.

Apabila program menemukan sebuah sub-directory, maka program akan tetap melakukan traverse ke dalam isi sub-
directory tersebut. Namun setelah semua isi sub-directory tersebut dikunjungi, program akan menghapus sub-directory tersebut dan tidak akan mengkategorikannya.

```c
void listFilesRecursively(char *basePath)
{
	char path_src[1000];
	struct dirent *dp;
	struct  stat statbuf;
	pthread_t t_id[200];
	int i=0;
	DIR *dir = opendir(basePath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0)
		{

			strcpy(path_src, basePath);
			strcat(path_src, "/");
			strcat(path_src, dp->d_name);

			listFilesRecursively(path_src);
			if (isDirectory(path_src))
			{
			rmdir(path_src);
			}
			else {
			pthread_create(&t_id[i], NULL, &auto_check, (void *)path_src);
			pthread_join(t_id[i], NULL);
			i++;
			}
		}
	}
	closedir(dir);
}
```

- `Nomor 3b` Untuk memerika ekstensi sebuah file dapat digunakan fungsi get_filename_ext. Pada fungsi ini, program diperbolehkan untuk mendapatkan string setelah sebuah karakter tertentu dengan menggunakan fungsi bawaan dari C, yaitu strrchr. Dalam kasus ini, porgram akan mengembalikan string setelah tanda '.' terakhir. Hal ini dikarenakan string dengan titik terakhir pada file (.exe, .png) merupakan informasi ekstensi file.
Untuk ekstensi file yang tidak diketahui, maka fungsi strrchr akan mengembalikan nilai NULL atau string yang sama dengan nama file karena tidak ditemukan tanda . Sehingga fungsi get_filename_ext akan mengembalikan string Unknown

Sedangkan untuk file yang hidden atau disembunyikan, file tersebut biasanya ditandai dengan tanda . di depan nama file. Maka dari itu, untuk file jenis ini, pemeriksaan yang digunakan adalah dengan menggunakan array of char dari nama file tersebut dengan merujuk pada indeks ke 0 (awal string). Apabila indeks ke 0 sebuah nama file sama dengan . maka fungsi get_filename_ext akan mengembalikan string Hidden

```c 
int is_hidden(const char *name)
{
	return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}

char *get_filename_ext(char *filename) {
	struct stat st;
	char *dot = strrchr(filename, '.');
	if (is_hidden(filename)) return "Hidden";
	if(!dot || dot == filename) return "Unknown";
	else return dot+1;
}
```

- `Nomor 3c` Fungsi yang digunakan dalam thread untuk pengkategorian file adalah auto_check dengan path file sebelum dikategorikan sebagai parameternya.
Di dalam fungsi auto_check, digunakan fungsi basename untuk memisahkan nama file dari pathnya sehingga dapat diperiksa ekstensi file tersebut dengan memanggil fungsi get_filename_ext seperti yang dijelaskan pada nomor 3b.
Kemudian thread terkait akan membuat sebuah directory baru sesuai dengan ekstensi file tersebut.
Untuk memindahkan file tersebut ke directory yang sesuai dengan ekstensinya, dapat digunakan fungsi rename dengan mengubah nama path file asalnya ke path file sesuai dengan directory ekstensi terkait yang sebelumnya sudah dibuat.

```c
void *auto_check(void *args) {
char path_des[1000];
	char *path_src = (char*)malloc(sizeof(char));
	path_src = (char*)args;
	char *bname = basename(path_src);
	char *ext =strdup(get_filename_ext(bname));
	//printf("%s\n", bname);
	stringLwr(ext);
	if(strcmp(ext,"gz")== 0) {
		ext = "tar.gz";
	}
	strcpy(path_des, "/home/gaudhiwaa/shift3/hartakarun");
	strcat(path_des, "/");
	strcat(path_des, ext);
	strcat(path_des, "/");
	safe_create_dir(path_des);
	strcat(path_des, bname);
	rename(path_src, path_des);
}
```

- `Nomor 3d` Untuk melakukan zip folder hartakarun, maka dapat digunakan fungsi execv() dengan terlebih dahulu memanggil sebuah proses baru. Proses baru dapat dipanggil dengan menggunakan bantuan fungsi fork().
Agar file tersebut dapat disimpan ke working directory dari client, maka sebuah directory baru bernama Client akan dibuat dengan bantuan mkdir dan argumen untuk fungsi execv() akan dituliskan sebagai berikut.

```c
child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/gaudhiwaa/Client/hartakarun.zip", "/home/gaudhiwaa/shift3/hartakarun", NULL};
		if(execvp("/bin/zip", argv) == -1) {
			perror("zip failed!");
			exit(EXIT_FAILURE);
		}
	}
```
- `Nomor 3e` Untuk mengirimkan file ke server, kita perlu terlebih dahulu membuat socket agar port server dengan port client terhubung dengan menggunakan outline program yang sudah disebutkan pada abstrak soal sebelumnya.
Dalam program client, untuk mengirimkan file hartakarun.zip, client harus terlebih dahulu menuliskan command "send hartakarun.zip". Apabila client memasukan string atau command lain, program akan keluar dengan menampilkan pernyataan bahwa command tidak sesuai.

```c
scanf("%[^\n]s", command);
	if(strcmp(command, "send hartakarun.zip") != 0) {
		printf("Invalid command, exiting program");
		return 0;
	}
```
Setelah menuliskan command "send hartakarun.zip" client akan mengirimkan file hartakarun.zip ke server.
![hasil](./screenshot/3a.png)
![hasil](./screenshot/3b.png)
![hasil](./screenshot/3c.png)
![hasil](./screenshot/3d.png)
![hasil](./screenshot/3e.png)
![hasil](./screenshot/3f.png)
